package autopark.dao.impl;

import autopark.dao.IRoleDAO;
import autopark.domain.Role;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;


@Repository
public class RoleDAOImpl extends RootDAOImpl<Role> implements IRoleDAO {

    public RoleDAOImpl() {
        super("autopark.domain.Role", Role.class);
    }

    public Role getUserRole() {
        final Query query = getSession().createQuery("from Role where authority=?").setString(0, Role.ROLE_USER);
        return (Role)query.uniqueResult();
    }
}