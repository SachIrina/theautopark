<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="/js/lib/bootstrap/v3_3_6/css/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/styles/signup.css" />
    <link rel="stylesheet" type="text/css" href="/styles/style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="/js/date.js"></script>


    </head>
    <body>
        <%@ include file="tiles/header_v2.jsp" %>

        <div class="container">
            <springForm:form action="/signup" method="post" commandName="command" name="loginForm" cssClass="form-signup">
                <h2 class="form-signin-heading">Регистрация аккаунта</h2>
                <br/>
                <c:choose>
                    <c:when test="${res==-2}">
                        <span class="red">Ошибка. Аккаунт с таким email уже существует.</span>
                        <br/>
                    </c:when>
                    <c:when test="${res==0}">
                        <span class="green">Ваш аккаунт успешно создан.</span>
                        <br/>
                    </c:when>
                </c:choose>

                <input name="name" type="text" placeholder="ФИО" class="form-control" value="${command.name}" >
                <div><springForm:errors path="name" cssClass="error red" /></div>

                <input name="email" type="text" placeholder="Ваш Email" class="form-control" value="${command.email}" >
                <div><springForm:errors path="email" cssClass="error red" /></div>

                <input name="phone" type="text" placeholder="Ваш телефон" class="form-control" value="${command.phone}">
                <div><springForm:errors path="phone" cssClass="error red" /></div>

                <input type="text" style="display:none" />
                <input name="birthday" type="text" placeholder="Дата рождения" class="datepicker form-control" autocomplete="off"
                       value='<fmt:formatDate value="${command.birthday}" pattern="dd.MM.yyyy" />'>
                <div><springForm:errors path="birthday" cssClass="error red" /></div>
                <input type="text" style="display:none" />

                <input name="password" type="password" placeholder="Пароль" class="form-control">
                <div><springForm:errors path="password" cssClass="error red" /></div>

                <input name="passwordRetry" type="password" placeholder="Повтор пароля" class="form-control">
                <div><springForm:errors path="passwordRetry" cssClass="error red" /></div>

                <button class="btn btn-lg btn-primary btn-block" type="submit">ОК</button>



            </springForm:form>


            </div>
        </div>








        <hr>
        <%@ include file="tiles/footer.jsp" %>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/js/lib/bootstrap/v3_3_6/js/bootstrap.min.js"></script>

    </body>
</html>