package autopark.domain;

import java.util.Date;

/**
 * Created by vclass16 on 08.02.2016.
 */
public enum Article {
    Petrov,
    Ivanov,
    Sidorov,
    Zaicev,
    Volkov,
    Skvorcov
}
