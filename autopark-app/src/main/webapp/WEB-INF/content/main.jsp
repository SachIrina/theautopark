<%--http://www.w3schools.com/bootstrap/bootstrap_tables.asp--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <!-- Bootstrap -->
        <link href="/js/lib/bootstrap/v3_3_6/css/bootstrap.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->


        <link rel="stylesheet" type="text/css" href="/styles/main.css" />
        <link rel="stylesheet" type="text/css" href="/styles/style.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <script src="/js/lib/jquery/jquery.i18n.properties-min-1.0.9.js"></script>
        <script src="/js/main.js"></script>


    </head>

    <body>
        <%@ include file="tiles/header_v2.jsp" %>

        <div class="container">

            <div class="row">
                <div id="filter-panel" class="filter-panel">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-inline" action="/searchcars" method="post">
                                <div class="form-group" title="${searchData.carVendor}">
                                    <label class="filter-col" style="margin-right:0;" for="sel-vendor">Марка ТС:</label>
                                    <select name="carVendor" class="form-control" id="sel-vendor" value="${searchData.carVendor}" >
                                        <option value="0">...</option>
                                    </select>
                                </div>
                                <div class="form-group" title="${searchData.carType}">
                                    <label class="filter-col" style="margin-right:0;" for="sel-type">Тип ТС:</label>
                                    <select name="carType" class="form-control" id="sel-type" value="${searchData.carType}" >
                                        <option value="0">...</option>
                                    </select>
                                </div>
                                <div class="form-group" title="${searchData.carCapacity}">
                                    <label class="filter-col" style="margin-right:0;" for="sel-capacity">Грузоподъемность ТС:</label>
                                    <select name="carCapacity" class="form-control" id="sel-capacity" value="${searchData.carCapacity}">
                                        <option value="0">...</option>
                                    </select>
                                </div>



                                <div class="form-group">
                                    <label class="filter-col" style="margin-right:0;" for="search-text">Текст:</label>
                                    <input name="searchText" type="text" class="form-control input-sm" id="search-text" value="${searchData.searchText}">
                                </div><!-- form group [search] -->

                                <%--
                                <div class="form-group">
                                    <div class="checkbox" style="margin-left:10px; margin-right:10px;">
                                        <label><input type="checkbox"> Remember parameters</label>
                                    </div>
                                    <button type="submit" class="btn btn-default filter-col">
                                        <span class="glyphicon glyphicon-record"></span> Save Settings
                                    </button>
                                </div>
                                --%>
                                <button type="submit" class="btn btn-primary" >
                                    <span class="glyphicon glyphicon-cog"></span> Найти
                                </button>
                                <a  class="btn btn-info" id="clear-filter">
                                    <span class=""></span> Сбросить фильтр
                                </a>

                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <div>
                <br/>
                <span class="title2">Автомобили</span>
                <br/><br/>
                <div>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Марка</th>
                            <th>Модель</th>
                            <th>Тип</th>
                            <th><fmt:message key="loading.capacity"/></th>
                            <th>Год выпуска</th>
                            <th>Описание</th>
                            <th>Имя Владельца</th>
                            <th>Email Владельца</th>
                            <th>Телефон Владельца</th>
                            <th>Цена</th>
                            <th>Цена за</th>
                            <th>Просмотр</th>
                        </tr>
                        </thead>
                        <c:if test="${cars != null}">
                            <tbody>
                            <c:forEach items="${cars}" var="item"  varStatus="s">
                                <tr class="${rowClass}">
                                    <td>
                                        <label>
                                            <a href="/car/${item.id}">${item.id}</a>
                                        </label>
                                    </td>
                                    <td>
                                        <label>${item.vendor}</label>
                                    </td>
                                    <td>
                                        <label>${item.model}</label>
                                    </td>
                                    <td>
                                        <label>${item.type}</label>
                                    </td>
                                    <td>
                                        <label>${item.capacity}</label>
                                    </td>
                                    <td>
                                        <label>${item.year}</label>
                                    </td>
                                    <td>
                                        <label>${item.description}</label>
                                    </td>
                                    <td>
                                        <label>${item.user.name} </label>
                                    </td>
                                    <td>
                                        <label>${item.user.email}</label>
                                    </td>
                                    <td>
                                        <label>${item.user.phone}</label>
                                    </td>
                                    <td>
                                        <c:if test="${item.priceDTO.price!=null}">
                                            <label>${item.priceDTO.price} <fmt:message key="currency.1"/> </label>
                                        </c:if>
                                    </td>
                                    <td>
                                        <c:if test="${item.priceDTO.price!=null}">
                                            <label><fmt:message key="${item.priceDTO.priceUnit}"/></label>
                                        </c:if>
                                    </td>
                                    <td>
                                        <a href="/car/${item.id}"><img width="16" height="16" src="/img/arrow1.png"></a>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </c:if>
                    </table>


                </div>

            </div>

        </div>



        <hr>
        <%@ include file="tiles/footer.jsp" %>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/js/lib/bootstrap/v3_3_6/js/bootstrap.min.js"></script>

    </body>
</html>