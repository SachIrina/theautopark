package autopark.service;

/**
 * Created by  01 on 15.02.2016.
 */
public interface IEmailSender {
    void sendEmail(String to, String content, String subject);
}
