package autopark.web.controller;

import autopark.dto.CarDTO;
import autopark.dto.CarSearchDTO;
import autopark.service.ICarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class SearchController {

    @Autowired
    @Qualifier("carServiceJdbc")
    private ICarService carServiceJdbc;


    @RequestMapping(value = {"/searchcars"})
    public String handle(@ModelAttribute CarSearchDTO dto, ModelMap modelMap) {
        List<CarDTO> list = carServiceJdbc.searchCars(dto);
        modelMap.put("cars",list);
        modelMap.put("searchData",dto);

        return "/WEB-INF/content/main.jsp";
    }








}



