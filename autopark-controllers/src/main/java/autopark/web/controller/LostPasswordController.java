package autopark.web.controller;

import autopark.dto.UserDTO;
import autopark.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by  01 on 14.03.2016.
 */
@Controller
public class LostPasswordController {

    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;

    @Autowired
    @Qualifier("emailValidator")
    private Validator validator;

    @RequestMapping(value = "/lostpassword", method = RequestMethod.GET)
    public String handle(ModelMap modelMap){
        return "/WEB-INF/content/lostpassword.jsp";
    }

    @RequestMapping(value = "/lostpassword", method = RequestMethod.POST)
    public String handlePost(@ModelAttribute("command") @Validated UserDTO dto,
                             BindingResult bindingResult, ModelMap modelMap, HttpServletRequest request){
        // .....

        String baseUrl = request.getScheme() + // "http"
                                    "://" +                                // "://"
                                    request.getServerName() +              // "myhost"
                                    ":" +                                  // ":"
                                    request.getServerPort() +              // "80"
                                    request.getContextPath();
        boolean valid = userService.processLostPassword(baseUrl,dto.getEmail());
        //......


        return "/WEB-INF/content/lostpassword.jsp";
    }

    @InitBinder
    private void binder(WebDataBinder binder) {
        //Add validator
        binder.setValidator(validator);
    }


}
