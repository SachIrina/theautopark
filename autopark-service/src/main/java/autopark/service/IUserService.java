package autopark.service;

import autopark.dto.UserDTO;

public interface IUserService {
    boolean createUser(UserDTO dto);
    UserDTO getUserByEmail(String email);

    /**
     * Метод принимает информацию о нашем сервере, email
     * Достает пользователя, создает ссылку, и посылает на этот email ссылку.
     * @param ourServer - server info
     * @param email - email пользователя
     * @return
     */
    boolean processLostPassword(String ourServer,String email);
}
