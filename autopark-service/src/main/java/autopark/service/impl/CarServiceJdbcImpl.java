package autopark.service.impl;

import autopark.dao.ICarDAO;
import autopark.dto.CarDTO;
import autopark.dto.CarSearchDTO;
import autopark.service.ICarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by  01 on 15.02.2016.
 */
@Service("carServiceJdbc")
public class CarServiceJdbcImpl implements ICarService{

    @Autowired
    private ICarDAO carDAO;

    public List<CarDTO> getCars() {
        return carDAO.getCarsJdbc();
    }

    public CarDTO getCar(Long id) {
        return null;
    }

    @Override
    public List<CarDTO> searchCars(CarSearchDTO dto) {
        return carDAO.searchCars(dto);
    }
}
