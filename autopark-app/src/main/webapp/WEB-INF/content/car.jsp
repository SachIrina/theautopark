<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/styles/car.css" />
    <link rel="stylesheet" type="text/css" href="/styles/style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="http://code.jquery.com/jquery-latest.js">
    </script>
</head>


<body>
<%@ include file="tiles/header.jsp" %>

<div class="mainarea">
    <div class="content">
        <div>
            Мой автомобиль
        </div>
        <div>
            <span title="Марка">Марка: ${car.vendor}</span>
        </div>
        <div>
            Модель: ${car.model}
        </div>
        <div>
            Год выпуска: ${car.year}
        </div>
        <div>
            Тип ТС: ${car.type}
        </div>
        <div>
            Тип ТС: ${car.capacity}
        </div>

    </div>
    <%@ include file="tiles/footer.jsp" %>
</div>
</body>




<a href="/">Главная</a>